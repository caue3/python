num = int(input())
pi = 3.14159
volume = (4.0/3) * pi * num ** 3
print('VOLUME = {:.3f}'.format(volume))